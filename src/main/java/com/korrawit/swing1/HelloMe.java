/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.korrawit.swing1;

import java.awt.Color;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JTextField;

class MyActionListener implements ActionListener {
    
    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("MyActionListener : Action");
    }
    
}

/**
 *
 * @author DELL
 */
public class HelloMe implements ActionListener {
    
    public static void main(String[] args) {
        JFrame frame = new JFrame("Hello Me");
        frame.setSize(500, 300);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        JLabel labelName = new JLabel("Your Name : ");
        labelName.setSize(80, 20);
        labelName.setLocation(5, 5);
        labelName.setBackground(Color.white);
        labelName.setOpaque(true);
        
        JTextField textfName = new JTextField();
        textfName.setSize(200, 20);
        textfName.setLocation(90, 5);
        
        JButton buttonHello = new JButton("Hello");
        buttonHello.setSize(200, 20);
        buttonHello.setLocation(90, 40);
        MyActionListener myActionLister = new MyActionListener();
        buttonHello.addActionListener(myActionLister);
        buttonHello.addActionListener(new HelloMe());
        
        ActionListener actionListener = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                System.out.println("Anonymous Class : Action");
            }
        };
        buttonHello.addActionListener(actionListener);
        
        JLabel labelHello = new JLabel("Hello...", JLabel.CENTER);
        labelHello.setSize(200, 20);
        labelHello.setLocation(90, 70);
        
        frame.setLayout(null);
        
        frame.add(labelName);
        frame.add(textfName);
        frame.add(buttonHello);
        frame.add(labelHello);
        
        buttonHello.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String name = textfName.getText();
                labelHello.setText("Hello " + name);
            }
        });
        
        frame.setVisible(true);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        System.out.println("HelloME : Action");
    }
}
