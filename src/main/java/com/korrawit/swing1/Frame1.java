/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.korrawit.swing1;

import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import javax.swing.JFrame;
import javax.swing.JLabel;

/**
 *
 * @author DELL
 */
public class Frame1 {
    
    public static void main(String[] args) {
        JFrame frame = new JFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);
        
        JLabel labelHelloWorld = new JLabel("Hello World!", JLabel.CENTER);
        labelHelloWorld.setBackground(Color.ORANGE);
        labelHelloWorld.setOpaque(true);
        labelHelloWorld.setFont(new Font("Verdana", Font.PLAIN, 25));
        frame.add(labelHelloWorld);
        frame.setSize(new Dimension(500, 300));
        
    }
    
}
